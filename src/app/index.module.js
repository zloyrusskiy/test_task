(function() {
  'use strict';

  angular
    .module('testTask', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ngMaterial','ngResource']);

})();
