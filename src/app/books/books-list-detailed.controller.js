(function() {
  'use strict';

  angular
    .module('testTask')
    .controller('BooksListDetailedController', BooksListDetailedController);

  /** @ngInject */
  function BooksListDetailedController(book, bundles) {
    var vm = this;

    vm.book = book;
    vm.bundles = bundles;
  }
})();