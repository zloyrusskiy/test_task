(function() {
  'use strict';

  angular
    .module('testTask')
    .controller('BooksListController', BooksListController);

  /** @ngInject */
  function BooksListController(books) {
    var vm = this;

    vm.books = books;
  }
})();