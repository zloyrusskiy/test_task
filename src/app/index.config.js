(function() {
  'use strict';

  angular
    .module('testTask')
    .config(config);

  /** @ngInject */
  function config($locationProvider) {
    $locationProvider.html5Mode(true);
  }

})();
