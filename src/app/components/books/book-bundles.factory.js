(function() {
  'use strict';

  angular
    .module('testTask')
    .factory('BookBundles', BookBundles);

  /** @ngInject */
  function BookBundles($resource, AGGREGATION_API_URI) {
    return $resource(AGGREGATION_API_URI + '/public/catalog/:book_id/bundles/:bundle_id');
  }
})();