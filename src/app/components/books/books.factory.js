(function() {
  'use strict';

  angular
    .module('testTask')
    .factory('Books', Books);

  /** @ngInject */
  function Books($resource, AGGREGATION_API_URI) {
    return $resource(AGGREGATION_API_URI + '/public/catalog/:id');
  }
})();