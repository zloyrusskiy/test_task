(function() {
  'use strict';

  angular
    .module('testTask')
    .directive('bookImage', bookImage);

  /** @ngInject */
  function bookImage() {
    return {
      restrict: 'E',
      scope: {
          'resourceId': '='
      },
      replace: true,
      template: '<div class="book-image"><img ng-if="resourceId" ng-src="{{ bi.imageFor(resourceId) }}" title="Изображение книги"></div>',
      controller: BookImageController,
      controllerAs: 'bi'
    };
  }

  /** @ngInject */
  function BookImageController($scope, BOOK_IMAGE_URI_TEMPLATE) {
    var vm = this;

    vm.imageFor = function (resourceId) {
      return BOOK_IMAGE_URI_TEMPLATE({ resourceId: resourceId});
    };
  }
})();