(function() {
  'use strict';

  angular
    .module('testTask')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        onEnter: function($state) {
          $state.go('books_list');
        }
      })
      .state('books_list', {
        url: '/books/',
        templateUrl: 'app/books/books-list.html',
        controller: 'BooksListController',
        controllerAs: 'bl',
        resolve: {
          books: function (Books) {
            return Books.query();
          }
        }
      })
      .state('books_list_detailed', {
        url: '/books/:id',
        templateUrl: 'app/books/books-list-detailed.html',
        controller: 'BooksListDetailedController',
        controllerAs: 'bld',
        resolve: {
          book: function (Books, $stateParams) {
            return Books.get({ id: $stateParams.id });
          },
          bundles: function (BookBundles, $stateParams) {
            return BookBundles.query({ book_id: $stateParams.id });
          }
        }
      })
      .state('notfound_404', {
        url: '/notfound_404',
        templateUrl: 'app/main/404.html'
      })

      ;

      $urlRouterProvider.otherwise('/notfound_404');
  }

})();
