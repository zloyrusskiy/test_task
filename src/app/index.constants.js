/* global _:false */
(function() {
  'use strict';

  angular
    .module('testTask')
    .constant('_', _)
    .constant('AGGREGATION_API_URI', 'https://ds.aggregion.com/api')
    .constant('BOOK_IMAGE_URI_TEMPLATE', _.template('https://storage.aggregion.com/api/files/<%= resourceId %>/shared/data'))

})();
